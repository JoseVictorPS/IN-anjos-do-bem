class AddReferenceToAnimalPhoto < ActiveRecord::Migration[5.2]
  def change
    add_reference :animal_photos, :animal, foreign_key: true
  end
end
