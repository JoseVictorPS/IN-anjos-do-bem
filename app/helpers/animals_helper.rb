module AnimalsHelper
    def human_sex(sex)
        hash = {male: 'Macho', female: 'Fêmea'}
        hash[sex.to_sym]
    end
end
